import 'package:flutter/material.dart';
import './src/screen/IntroPage.dart';

void main()async{
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Intro layout',
      home: IntroPage(),
  ));
}
