class Ayat{

  String ar;
  String id;
  String nomor;
  String tr;

Ayat({
  this.ar,this.id,this.nomor,this.tr
});

factory Ayat.fromJson(Map<String, dynamic > json){
  return Ayat(
    ar: json['ar'] as String,
    id: json['id'] as String,
    nomor: json['nomor'] as String,
    tr: json['tr'] as String,
  );
}

}