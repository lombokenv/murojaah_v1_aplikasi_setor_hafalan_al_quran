class Pengujian{
  String dataUji;
  String dataApi;
  Pengujian({this.dataUji,this.dataApi});

  String getDataUji(){
    return dataUji;
  }
  String getDataApi(){
    return dataApi;
  }

  String getResult(){
    String  result="";
    if(getDataUji().substring(0,4).toUpperCase() == getDataApi().substring(0,4).toUpperCase()
        || getDataUji().substring(getDataUji().length-4,getDataUji().length).toUpperCase() == getDataApi().substring(getDataApi().length-4,getDataApi().length).toUpperCase()
      ){
          result = "Alhamdulillah Pelafalan Anda Bagus :)";
      }else{
        result = "Hmmm....\n"+
        "Bacaan Anda kurang bagus\n Yuk Ulangi Lagi :)";
      }

    return result;
  }
}