import 'dart:convert';
import 'package:http/http.dart' as http;
import './alquran.dart';
import './ayat.dart';


class Services{
  static const String url = "https://al-quran-8d642.firebaseio.com/data.json?print=pretty";


  static Future<List<AlQuran>> getArti()async{
    try{  
        final response = await http.get(url);
        if(response.statusCode == 200){
          List<AlQuran> list = parseArti(response.body);
          return list;
        }else{
          throw Exception("Error");
        }
    }catch(e){
      throw Exception(e.toString());
    }
  }

  static List<AlQuran> parseArti(String responseBody){
    final parsed = json.decode(responseBody)
    .cast<Map<String, dynamic>>();
    return parsed.map<AlQuran>((json) => AlQuran.fromJson(json))
    .toList();
  }


  //untuk ayat
  static Future<List<Ayat>> getAyat(String surah)async{
      final String urlAyat = "https://al-quran-8d642.firebaseio.com/surat/${surah}.json?print=pretty";
      try{
          final response = await http.get(urlAyat);
          if(response.statusCode == 200){
          List<Ayat> list = parseAyat(response.body);
          return list;
        }else{
          throw Exception("Error");
        }
      }catch(e){
        throw Exception(e.toString());
      }
  }

    static List<Ayat> parseAyat(String responseBody){
    final parsed = json.decode(responseBody)
    .cast<Map<String, dynamic>>();
    return parsed.map<Ayat>((json) => Ayat.fromJson(json))
    .toList();
  }

}