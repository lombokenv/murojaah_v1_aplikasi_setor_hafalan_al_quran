class AlQuran{

  String arti;
  String asma;
  String audio;
  int ayat;
  String keterangan;
  String nama;
  String nomor;
  String type;

AlQuran({
  this.arti,this.asma,this.audio,this.ayat,
  this.keterangan,this.nama,this.nomor,this.type
});

factory AlQuran.fromJson(Map<String, dynamic > json){
  return AlQuran(
    arti: json['arti'] as String,
    asma: json['asma'] as String,
    audio: json['audio'] as String,
    ayat: json['ayat'] as int,
    keterangan: json['keterangan'] as String,
    nama: json['nama'] as String,
    nomor: json['nomor'] as String,
    type: json['type'] as String,

  );
}

}