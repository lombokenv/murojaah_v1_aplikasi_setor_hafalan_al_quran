import 'package:flutter/material.dart';

class InfoApp extends StatefulWidget{

      @override
      _InfoAppState createState() => new _InfoAppState();
}

class _InfoAppState extends State<InfoApp> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: 
        AppBar(
                elevation: 0,
                backgroundColor: Colors.purple,
                automaticallyImplyLeading: true,
                iconTheme: IconThemeData(
                  color: Colors.white
                ),
                centerTitle: true,
                title: new Text(
                  'Tentang Aplikasi',
                  style: new TextStyle(
                    fontFamily: "Montserrat",
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 18.0
                  ),
                ),
            ),
      body: SingleChildScrollView(
        child: new Container(
            width: MediaQuery.of(context).size.width,
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.all(10.0),
                  width: MediaQuery.of(context).size.width,
                  height: 200.0,
                  child: new Image.asset("assets/images/about.jpg",fit: BoxFit.fill,),
                ),
                new Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: new Text(
                    "بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيمِ",
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: new Text("Murojaah adalah aplikasi stor hafalan Al-Qur'an"+
                    " Agar memudahkan adik-adik dalam menghafal ayat-ayat Al-Qur'an. Inovasi Murojaah berawal"+
                    " dari banyak adik-adik yang tidak memperhatikan pelafalan ayat Al-Qur'an dengan benar" +
                    " sehingga mengkhawatirkan perbedaan makna dengan ayat yang dibaca."+
                    " Murojaah hadir untuk memudahkan adik-adik menghafal sekaligus memanfaatkan"
                    " teknologi Smartphone yang dilengkapi dengan Google Spech agar agar lebih interaktif"+
                    " dalam menghafal ayat-ayat Al-Qur'an.",
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 15,
                      
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Semoga harapan untuk kedepannya Murojaah menjadi aplikasi"+
                    " yang mendatangkan manfaat dan kebaikan untuk adik-adik sekalian. Aamin Ya Rabbalalammin",
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 15,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                new Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Version : v0.1 Murojaah",
                              style: TextStyle(
                                fontFamily: "Montserrat",
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Support : Android & iOS",
                              style: TextStyle(
                                fontFamily: "Montserrat",
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Pengembang : Rhomy Idris Sardi & Budiman Rabbani",
                              style: TextStyle(
                                fontFamily: "Montserrat",
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                ),
              ],
            ),
          ),
      )
      ) ;
  }

}
