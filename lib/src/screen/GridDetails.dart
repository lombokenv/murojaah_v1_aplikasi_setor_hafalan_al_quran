import 'package:flutter/material.dart';
import '../model/alquran.dart';
import '../model/ayat.dart';
import '../model/services.dart';
import 'dart:async';
import 'package:audio/audio.dart';
import './voicehome.dart';

class GridDetails extends StatefulWidget {
  final AlQuran curAlquran;
   String url;
  
  GridDetails({@required this.curAlquran, @required this.url});
  @override
  GridDetailsState createState() => GridDetailsState();
}

class GridDetailsState extends State<GridDetails> {
  List<String>latin= new List<String>();

  StreamController<int> streamController = new StreamController<int>();

  //start sounds
    Audio audioPlayer = new Audio(single: true);
    AudioPlayerState state = AudioPlayerState.STOPPED;
    double position = 0;
    StreamSubscription<AudioPlayerState> _playerStateSubscription;
    StreamSubscription<double> _playerPositionController;
    StreamSubscription<int> _playerBufferingSubscription;
    StreamSubscription<AudioPlayerError> _playerErrorSubscription;
  //end sounds

  //initstate
    @override
    void initState()
    {
        _playerStateSubscription = audioPlayer.onPlayerStateChanged.listen((AudioPlayerState state)
        {
            print("onPlayerStateChanged: ${audioPlayer.uid} $state");

            if (mounted)
                setState(() => this.state = state);
        });

        _playerPositionController = audioPlayer.onPlayerPositionChanged.listen((double position)
        {
            print("onPlayerPositionChanged: ${audioPlayer.uid} $position ${audioPlayer.duration}");

            if (mounted)
                setState(() => this.position = position);
        });

        _playerBufferingSubscription = audioPlayer.onPlayerBufferingChanged.listen((int percent)
        {
            print("onPlayerBufferingChanged: ${audioPlayer.uid} $percent");
        });

        _playerErrorSubscription = audioPlayer.onPlayerError.listen((AudioPlayerError error)
        {
            throw("onPlayerError: ${error.code} ${error.message}");
        });

        audioPlayer.preload(widget.url);

        super.initState();
    }
  //end initstate

 
 
  listView(AsyncSnapshot<List<Ayat>> snapshot){
    return Padding(
      padding: EdgeInsets.all(5.0),
      child: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, iterasi){
          return new Container(
            width: MediaQuery.of(context).size.width,
          
            child: new Column(
              children: snapshot.data.map(
                (ayat){
                  latin.add(ayat.tr);
                  return new Column(
                    children: <Widget>[
                      new Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                  ayat.ar,
                                  style: TextStyle(
                                        fontSize: 34.0,
                                        fontWeight: FontWeight.bold
                                  ),
                                      textAlign: TextAlign.right,
                              ),
                      ),

                      new Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: new Text(
                          "${ayat.nomor}. "+ayat.id,
                          style: TextStyle(
                            fontSize: 16.0
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        ),
                      )
                    ],
                  );
                },
              ).toList(),
            )
          );
        },
      ),
    );
  }


  circularProgress() {
    return Center(
      child: const CircularProgressIndicator(),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body : new NestedScrollView(
              
              headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
                return <Widget>[                 
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: Colors.purple,
                     expandedHeight: 157.0,
                     titleSpacing: 0,
                     flexibleSpace: new FlexibleSpaceBar(
                       centerTitle: true,
                       title: new Wrap(
                          direction: Axis.vertical,
                          children: <Widget>[
                            new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Center(
                                child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text(widget.curAlquran.nama,
                                            style: new TextStyle(
                                           color: Colors.white,
                                            fontSize: 25.0,
                                            fontFamily: "Ahlan",
                                            fontWeight: FontWeight.bold,

                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                ),
                              ]
                            )
                          ]
                       ),
                       background: new Image.asset(
                         "assets/images/gridview.jpg",
                         fit: BoxFit.cover,
                       ),
                     ),
                     automaticallyImplyLeading: true,
                     pinned: true,
                     floating: false,
                     forceElevated: boxIsScrolled,
                  ),
                  SliverPersistentHeader(
                      pinned: true,
                      delegate: _SliverAppBarDelegate(
                        child: PreferredSize(
                        preferredSize: Size.fromHeight(42.0), 
                        child: new Material(
                          elevation: 1.0,
                          child: Container(
                            color:Colors.black54,
                            width: MediaQuery.of(context).size.width,
               
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                child: new RaisedButton(
                                  color: Colors.red,
                                  elevation: 0.0,
                                  onPressed: (){
                                    onPlay();
                                  },
                                  child: new Center(
                                    child: new Icon(Icons.play_arrow,color: Colors.white,size: 30,)
                                  ),
                                ),
                                ),
                                Flexible(
                                child: new RaisedButton(
                                  color: Colors.orangeAccent,
                                  elevation: 0.0,
                                  onPressed: (){
                                    onPause();
                                  },
                                  child: new Center(
                                    child: new Icon(Icons.pause, color:Colors.white,size: 30,)
                                  ),
                                ),
                                ),
                                Flexible(
                                 child: new RaisedButton(
                                  color: Colors.black,
                                  elevation: 0.0,
                                  onPressed: (){
                                    onSeek(0.0);
                                  },
                                  child: new Center(
                                    child: new Icon(Icons.stop, color:Colors.white,size: 30,)
                                  ),
                                ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        )
                      )
                      ),
                    ),
                ];
              },
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                         'Total Ayat : ${widget.curAlquran.ayat}', 
                         style: TextStyle(                         
                         fontWeight: FontWeight.bold,
                         fontFamily: "Montserrat",color: Colors.grey, fontSize: 18.0),
                         textAlign: TextAlign.center,
                    ),
                  ),
                  Flexible(
                    child: FutureBuilder<List<Ayat>>(
                      future: Services.getAyat(widget.curAlquran.nomor),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return Text('Error ${snapshot.error}');
                        }
                        //
                        if (snapshot.hasData) {
                          streamController.sink.add(snapshot.data.length);
                          // gridview
                          return listView(snapshot);

                        }

                        return circularProgress();
                      },
                    ),
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50.0,
                    child: new RaisedButton(
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) 
                        => VoiceHome(namaSurah: "${widget.curAlquran.nama}",
                           nomorSurah: "${widget.curAlquran.nomor}",
                           ayat: latin,
                           jumlahAyat: "${widget.curAlquran.ayat}",
                        )));
                      },
                      color: Colors.purple,
                      child: new Text(
                        'Uji Hafalan',
                        style: TextStyle(
                          fontSize: 21,
                          color: Colors.white
                        ),
                        ),
                    ),
                  )
                ],
              ),
              )
    );
  }


  //sounds atributes
    @override
    void dispose()
    {
        _playerStateSubscription.cancel();
        _playerPositionController.cancel();
        _playerBufferingSubscription.cancel();
        _playerErrorSubscription.cancel();
        audioPlayer.release();
        super.dispose();
    }

    onPlay()
    {
        audioPlayer.play(widget.url);
    }

    onPause()
    {
        audioPlayer.pause();
    }

    onSeek(double value)
    {
        // Note: We can only seek if the audio is ready
        audioPlayer.seek(value);
        audioPlayer.pause();
    }
  //end sounds

}


class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}