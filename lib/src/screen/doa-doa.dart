import 'dart:ui';
import '../model/doa-doa.dart';
import 'package:flutter/material.dart';
import '../screen/doa-doa/doa_sebelum_wudhu.dart';
import './doa-doa/doa_setelah_wudhu.dart';
import './doa-doa/doa_makan.dart';
import './doa-doa/doa_setelah_makan.dart';
import './doa-doa/doa_sebelum_tidur.dart';
import './doa-doa/doa_setelah_bangun.dart';
import './doa-doa/doa_masuk_kamar_mandi.dart';
import './doa-doa/doa_keluar_kamar_mandi.dart';
import './doa-doa/doa_masuk_rumah.dart';
import './doa-doa/doa_keluar_rumah.dart';
import './doa-doa/doa_masuk_masjid.dart';
import './doa-doa/doa_keluar_masjid.dart';
import './doa-doa/doa_setelah_istinja.dart';


class DoaState extends StatefulWidget{
  @override
  _DoaPageState createState() => _DoaPageState();
}

class _DoaPageState extends State<DoaState> {

  final List<DoaDoaModel> doaList=[
    DoaDoaModel(
      title: 'Doa Sebelum Wudhu',
      actions: DoaSebelumWudhu()
    ),
    DoaDoaModel(
      title: 'Doa Setelah Wudhu',
      actions: DoaSetelahWudhu()
    ),
    DoaDoaModel(
      title: 'Doa Sebelum Makan dan Minum',
      actions: DoaMakan()
    ),
    DoaDoaModel(
      title: 'Doa Setelah Makan dan Minum',
      actions: DoaSetelahMakan()
    ),
    DoaDoaModel(
      title: 'Doa Sebelum Tidur',
      actions: DoaSebelumTidur()
    ),
    DoaDoaModel(
      title: 'Doa Setelah Bangun',
      actions: DoaSetelahBangun()
    ),
    DoaDoaModel(
      title: 'Doa Masuk Kamar Mandi',
      actions: DoaMasukKamarMandi()
    ),
    DoaDoaModel(
      title: 'Doa Keluar Kamar Mandi',
      actions: DoaKeluarKamarMandi()
    ),
    DoaDoaModel(
      title: 'Doa Masuk Rumah',
      actions: DoaMasukRumah()
    ),
    DoaDoaModel(
      title: 'Doa Keluar Rumah',
      actions: DoaKeluarRumah()
    ),
    DoaDoaModel(
      title: 'Doa Masuk Masjid',
      actions: DoaMasukMasjid()
    ),
    DoaDoaModel(
      title: 'Doa Keluar Masjid',
      actions: DoaKeluarMasjid()
    ),
    DoaDoaModel(
      title: 'Doa Membuang Hadats Kecil',
      actions: DoaSetelahIstinja()
    ),
  ];

  Widget buildItems([EdgeInsetsGeometry margin]){
    return Expanded(
      child: Container(
          margin: margin,
          child: Column(
            children: _buildPage()
          ),
      ),
    );
  }


List<Widget> _buildPage(){
    List<Widget> widgets = [];
    for(int i = 0; i < doaList.length; i++){
       DoaDoaModel abp = doaList[i];
       widgets.add(
          new Container(
              decoration: new BoxDecoration(
              border: new Border(
              bottom: BorderSide(
                        color: Colors.grey[200]
                      )
                    )
              ),
              child: new SizedBox(
                    width: double.infinity,
                    child: new RaisedButton(
                      elevation: 0,
                      child: new Align(
                        alignment: Alignment.bottomLeft,
                        child: new Row(
                          children: [
                              new Text('#${i+1}  ',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.purple,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              new Text(abp.title,
                                style: TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              Expanded(
                                child: new Text(""),
                              ),
                              new Icon(Icons.keyboard_arrow_right,
                                color: Colors.purpleAccent,
                              ) //item title
                          ],
                        ),
                      ),
                      onPressed:(){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)
                            => abp.actions
                          )
                        );
                      },
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 22,bottom: 22,left: 10,right: 10),
                      splashColor: Colors.grey,

                    ),
                ),
            )
       );
    }
    print(doaList.length);
    return widgets;
  }


  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              elevation: 0,
              automaticallyImplyLeading: true,
              backgroundColor: Colors.purple,
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("Doa-Doa",
                      style: TextStyle(
                        shadows: <Shadow>[
                                Shadow(
                                  offset: Offset(0.4, 1.0),
                                  blurRadius: 3.0,
                                  color: Colors.black54,
                                ),
                              ],
                        color: Colors.white,
                        fontSize: 25.0,
                        fontFamily: "Ahlan",
                        fontWeight: FontWeight.bold,
                      )),
             background: Image.asset(
               "assets/images/doas1.jpg",
               fit: BoxFit.cover,
             ),    
                  ),
            ),

        SliverPersistentHeader(
          pinned: true,
          delegate: _SliverAppBarDelegate(
            child: PreferredSize(
            preferredSize: Size.fromHeight(40.0), 
            child: new Material(
              elevation: 1.0,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/bg2.jpg'),
                    fit: BoxFit.fill
                  )
                ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Doa-Doa Pilihan', 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "Montserrat",color: Colors.white, fontSize: 18.0),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
            )
          )
          ),
        ),

          ];
        },
      body: new Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: new Column(
            children: _buildPage()
          ),
        )
      )

          ),

        );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}