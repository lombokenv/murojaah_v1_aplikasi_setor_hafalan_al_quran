import 'package:flutter/material.dart';
import 'package:speech_recognition/speech_recognition.dart';
import 'package:permission_handler/permission_handler.dart';
import "package:html/parser.dart";
import '../model/pengujian.dart';


class VoiceHome extends StatefulWidget {
  String nomorSurah;
  String namaSurah;
  var ayat;
  var jumlahAyat;
  
  @override
  _VoiceHomeState createState() => _VoiceHomeState();
  VoiceHome({
    @required this.nomorSurah,
    @required this.namaSurah,
    @required this.ayat,
    @required this.jumlahAyat
  });
}

class _VoiceHomeState extends State<VoiceHome> {
  int i=0;
  final _formKey = GlobalKey<FormState>();
  
  final _voiceRecordController = TextEditingController();
 
  @override
  void dispose() {
    _voiceRecordController.dispose();
    super.dispose();
    
  }


  SpeechRecognition _speechRecognition;
  bool _isAvailable = false;
  bool _isListening = false;
  bool popup = false;


  String resultText = "";
  String _parseHtmlString(String htmlString) {

  var document = parse(htmlString);

  String parsedString = parse(document.body.text).documentElement.text;

  return parsedString;
  }

  @override
  void initState() {
    super.initState();
    askForPermissions();
    initSpeechRecognizer();
  }
    Future askForPermissions() async {
        Map<PermissionGroup, PermissionStatus> permissions = await PermissionHandler().requestPermissions([PermissionGroup.microphone]);
}

  void initSpeechRecognizer() {
    _speechRecognition = SpeechRecognition();

    _speechRecognition.setAvailabilityHandler(
      (bool result) => setState(() => _isAvailable = result),
    );

    _speechRecognition.setRecognitionStartedHandler(
      () => setState(() => _isListening = true),
    );

    _speechRecognition.setRecognitionResultHandler(
      (String speech) => setState(() { resultText = speech;
      }),
    );

    _speechRecognition.setRecognitionCompleteHandler(
      () => setState(() { _isListening = false;
          popup = true;
          
      }
      ),
    );
    _speechRecognition.activate().then(
          (result) => setState(() => _isAvailable = result),
        );
  }



  @override
  Widget build(BuildContext context) {


  Widget benar = Center(
        child: RaisedButton(
          color: Colors.purple,
          onPressed: () {
            return showDialog(
                context: context,
                builder: (BuildContext context) {

                  var hasilUji = Pengujian(dataUji: _voiceRecordController.text,dataApi: '${_parseHtmlString(widget.ayat[i])}');
                  
                  if(_voiceRecordController.text == ""){
                    return AlertDialog(
                              content: Text(" Silahkan cek kembali dan tekan voice ya"),
                    );

                  }
                  
                  else{
                    if(i > int.parse("${widget.jumlahAyat}")){
                        Navigator.pop(context);
                    }
                    if(hasilUji.getResult() == "Alhamdulillah Pelafalan Anda Bagus :)"){
                        i+=1;
                    }
                      return AlertDialog(
                        actions: <Widget>[
                              new Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 0.0),
                                      child: RaisedButton(
                                          onPressed: (){
                                            Navigator.pop(context);
                                            
                                          },
                                          child: Text("Keluar",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: "NotoSans"
                                                  
                                              )
                                          ),
                                          padding:EdgeInsets.only(left: 25,right: 25,top: 13,bottom: 13),
                                          textColor: Colors.white,
                                          color: Colors.purple,
                                      ),
                            ),

                        ],
                          content: new Container(
                                    child: new Wrap(
                                    direction: Axis.horizontal,
                                      children: <Widget>[
                                        new Padding(
                                          padding: EdgeInsets.only(bottom: 10.0),
                                          child: new Text(
                                              "Hasil Pengujian :",
                                              style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w600
                                              ),
                                            ),
                                        ),
                                        new Container(
                                          width: MediaQuery.of(context).size.width,
                                          child:  new Text('${hasilUji.getResult()}'),
                                        )
                                      ],
                              
                                  ),
                                  )

                        );
                  }
                });
          },
          child: Text("Cek Hafalan",
          style: TextStyle(
            fontFamily: "Montserrat",
            color: Colors.white,
            fontWeight: FontWeight.bold
          ),
          ),
        ),
      );

    if(popup){
      _voiceRecordController.text = resultText;
    }

    int z = i;

    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(0.0),
          gradient: new LinearGradient(
            colors: [
              Color(0xFFF48FB1),

              Colors.purple,
            ],
            begin: Alignment.centerLeft,
            end: new Alignment(1.0,1.0), 
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text("Surah: ${widget.namaSurah} Ayat: ${z+=1} \n",
            
            // "Ayat ${_parseHtmlString(widget.ayat[i])}",
            // "Ayat ${widget.ayat[i]}",
            style: TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
               
                FloatingActionButton(
                  child: Icon(Icons.mic),
                  onPressed: () {
                    if (_isAvailable && !_isListening)
                      _speechRecognition
                          .listen(locale: "in_ID")
                          .then((result) => print('$result'));
                  },
                  backgroundColor: Colors.pink,
                ),
                
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom:14.0),),
            new Form(
              key: _formKey,
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.purple[100],
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 12.0,
                  ),
                  
                  child: TextFormField(
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      hintText: 'Mulai voice sekarang!'
                    ),
                    validator: (value){
                    if(value.isEmpty){
                        return 'Mulai voice sekarang!';
                      }
                      return null;
                    },

                    enabled: false,
                    controller: _voiceRecordController,
                    style: TextStyle(fontSize: 18.0),
                  ),
                  
            ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            benar

          ],
        ),
      ),
    );
  }
}
