import 'dart:ui';

import 'package:flutter/material.dart';
import './info_apps.dart';
import './doa-doa.dart';
import './list_jus.dart';




class HomeState extends StatefulWidget{
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomeState> {

  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
        color: Colors.grey[200],
        child:NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.purple,
              expandedHeight: 165.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("Murojaah",
                      style: TextStyle(
                        shadows: <Shadow>[
                                Shadow(
                                  offset: Offset(0.4, 1.0),
                                  blurRadius: 3.0,
                                  color: Colors.black54,
                                ),
                              ],
                        color: Colors.white,
                        fontSize: 25.0,
                        fontFamily: "Ahlan",
                        fontWeight: FontWeight.bold,
                      )
                  ),
                  background: Image.asset(
                    "assets/images/doas.jpg",
                    fit: BoxFit.cover,
                  )
                  ),
            ),

        SliverPersistentHeader(
          pinned: true,
          delegate: _SliverAppBarDelegate(
            child: PreferredSize(
            preferredSize: Size.fromHeight(42.0), 
            child: new Material(
              elevation: 1.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/bg2.jpg'),
                    fit: BoxFit.fill
                  )
                ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'PILIHAN MENU', 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "Montserrat",color: Colors.white, fontSize: 18.0),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
            )
          )
          ),
        ),

          ];
        },
      body:  
        GridView.count(
        crossAxisCount: 1,
        padding: EdgeInsets.all(16.0),
        childAspectRatio: 2.2,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        children: <Widget>[
          GestureDetector(
          child: myGridItems(Icons.book,"(Stor Hafalan)", "assets/images/alquran.jpg", 0xFFEF9A9A, 0xFFE57373),
          onTap: (){
             //navigator 1
             Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListJus()));
          },
          ),
          GestureDetector(
          child: myGridItems(Icons.book,"(Doa-Doa)", "assets/images/doa1.jpg", 0xFFF48FB1, 0xFFF06292),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>DoaState()));
              },
             ),
          GestureDetector(
          child: myGridItems(Icons.info,"(Tentang Aplikasi)", "assets/images/doa-doa.jpg", 0xFFF48FB1, 0xFFF06292),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>InfoApp()));
            //navigator 1

              },
             ),
            ],
          ),

          ),
        ),
        );
  }

Widget myGridItems(var icon,String gridName, String gridimage, int color, int color1){
  return new Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(24.0),
      gradient: new LinearGradient(
        colors: [
          Color(color),

          Color(color1),
        ],
        begin: Alignment.centerLeft,
        end: new Alignment(1.0,1.0), 
      ),
    ),

    child: Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              image: DecorationImage(
                image: ExactAssetImage(gridimage),
                fit: BoxFit.fill,
              )
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10.0,),
                  Container(child: Icon(icon,color: Colors.white,),),
                  SizedBox(width: 10.0,),
                  Container(child: Text("Murojaah", 
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 23.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Montserrat",
                        ),),),
                  SizedBox(width: 10.0,),
                  ],),),
                  Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(gridName,style: TextStyle(
                        color:Colors.white,
                        fontSize: 20.0, 
                        fontWeight: FontWeight.bold,
                        shadows: <Shadow>[
                                Shadow(
                                  offset: Offset(0.4, 1.0),
                                  blurRadius: 6.0,
                                  color: Colors.black54,
                                ),
                              ], 
                   )),
                  ),
          ],
        ),
      ],
    ),
  );
}
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}