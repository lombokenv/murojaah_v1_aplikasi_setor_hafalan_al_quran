import 'package:flutter/material.dart';
import './doa_keluar_rumah.dart';

class DoaMasukRumah extends StatefulWidget{
  @override
  _DoaMasukRumahState createState() => _DoaMasukRumahState();
}

class _DoaMasukRumahState extends State<DoaMasukRumah> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              elevation: 0,
              automaticallyImplyLeading: true,
              backgroundColor: Colors.purple[600],
              
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,  
                  title: Text("Doa Masuk Rumah",  
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontFamily: "Montserrat",
                        fontWeight: FontWeight.bold,
                  )),  
                  ),
            ),

        SliverPersistentHeader(
          pinned: true,
          delegate: _SliverAppBarDelegate(
            child: PreferredSize(
            preferredSize: Size.fromHeight(40.0), 
            child: new Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/bottom.png'),
                    fit: BoxFit.fill
                  )
                ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '#9', 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "Montserrat",color: Colors.white, fontSize: 24.0),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
          )
          ),
        ),

          ];
        },
      body: new Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: new SingleChildScrollView(
        child: new Column(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(10.0),
                child: new Material(
                  elevation: 0.6,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    child: new Column(
                      children: <Widget>[
                          new Text('اَللّٰهُمَّ اِنِّىْ اَسْأَلُكَ خَيْرَالْمَوْلِجِ وَخَيْرَالْمَخْرَجِ بِسْمِ اللهِ وَلَجْنَا وَبِسْمِ اللهِ خَرَجْنَا وَعَلَى اللهِ رَبِّنَا تَوَكَّلْنَا',
                            style: TextStyle(
                              fontSize: 38.0,
                              fontWeight: FontWeight.bold
                            ),
                            textAlign: TextAlign.right,
                          ),
                          new Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Text(
                            'Bacaan dalam latin:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Montserrat"
                            ),
                            textAlign: TextAlign.left,
                            ),
                          ),
                          new Text(
                            'Allahumma innii as-aluka khoirol mauliji wa khoirol makhroji bismillaahi wa lajnaa wa bismillaahi khorojnaa wa’alallohi robbina tawakkalnaa',
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontSize: 16.0
                            ),
                            textAlign: TextAlign.justify,
                          ),
                          new Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Text(
                            'Terjamahan arti doa masuk rumah:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Montserrat"
                            ),
                            textAlign: TextAlign.left,
                            ),
                          ),
                          new Text(
                            "Ya Allah, sesungguhnya aku mohon kepada-Mu baiknya tempat masuk dan baiknya tempat keluar dengan menyebut nama Allah kami masuk, dan dengan menyebut nama Allah kami keluar dan kepada Allah Tuhan kami, kami bertawakkal",
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontSize: 16.0
                            ),
                            textAlign: TextAlign.justify,
                          )
                      ],
                    )
                  ),
                ),
              ),
              const SizedBox(height: 30),
              new Padding(
                padding: EdgeInsets.only(bottom: 40.0),
                child: RaisedButton(
                        onPressed: () {
                            Navigator.pop(context,true);// It worked for me instead of above line
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DoaKeluarRumah()),);
                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF6A1B9A),
                                Color(0xFF8E24AA),
                                Color(0xFFAB47BC),

                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: Wrap(
                            direction: Axis.horizontal,
                            children: <Widget>[
                                const Text(
                                  'Berikutnya  ',
                                style: TextStyle(fontSize: 20),
                                ),
                                new Icon(
                                  Icons.arrow_right,
                                  size: 25.0,
                                )
                            ],
                          ),
                        ),
                      ),
              )

            ],
          ),
      ),
      )

          ),

        );
  }

}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}