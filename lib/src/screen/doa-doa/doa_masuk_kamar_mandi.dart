import 'package:flutter/material.dart';
import './doa_keluar_kamar_mandi.dart';

class DoaMasukKamarMandi extends StatefulWidget{
  @override
  _DoaMasukKamarMandiState createState() => _DoaMasukKamarMandiState();
}

class _DoaMasukKamarMandiState extends State<DoaMasukKamarMandi> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              elevation: 0,
              automaticallyImplyLeading: true,
              backgroundColor: Colors.purple[600],
              
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,  
                  title: Text("Doa Masuk Kamar Mandi",  
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontFamily: "Montserrat",
                        fontWeight: FontWeight.bold,
                  )),  
                  ),
            ),

        SliverPersistentHeader(
          pinned: true,
          delegate: _SliverAppBarDelegate(
            child: PreferredSize(
            preferredSize: Size.fromHeight(40.0), 
            child: new Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/bottom.png'),
                    fit: BoxFit.fill
                  )
                ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '#7', 
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "Montserrat",color: Colors.white, fontSize: 25.0),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
          )
          ),
        ),

          ];
        },
      body: new Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: new SingleChildScrollView(
        child: new Column(
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.all(10.0),
                child: new Material(
                  elevation: 0.6,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    child: new Column(
                      children: <Widget>[
                          new Text('اَللّٰهُمَّ اِنِّيْ اَعُوْذُبِكَ مِنَ الْخُبُثِ وَالْخَبَآئِثِ',
                            style: TextStyle(
                              fontSize: 38.0,
                              fontWeight: FontWeight.bold
                            ),
                            textAlign: TextAlign.right,
                          ),
                          new Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Text(
                            'Bacaan dalam latin:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Montserrat"
                            ),
                            textAlign: TextAlign.left,
                            ),
                          ),
                          new Text(
                            'Alloohumma Innii A’uudzubka Minal Khubutsi Wal Khobaaitsi',
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontSize: 16.0
                            ),
                            textAlign: TextAlign.justify,
                          ),
                          new Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Text(
                            'Terjamahan arti doa masuk kamar mandi:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              fontFamily: "Montserrat"
                            ),
                            textAlign: TextAlign.left,
                            ),
                          ),
                          new Text(
                            "Ya Allah, sesungguhnya aku berlindung kepadamu dari segala kejahatan dan kotoran",
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontSize: 16.0
                            ),
                            textAlign: TextAlign.justify,
                          )
                      ],
                    )
                  ),
                ),
              ),
              const SizedBox(height: 30),
              new Padding(
                padding: EdgeInsets.only(bottom: 40.0),
                child: RaisedButton(
                        onPressed: () {
                            Navigator.pop(context,true);// It worked for me instead of above line
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DoaKeluarKamarMandi()),);
                        },
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF6A1B9A),
                                Color(0xFF8E24AA),
                                Color(0xFFAB47BC),

                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: Wrap(
                            direction: Axis.horizontal,
                            children: <Widget>[
                                const Text(
                                  'Berikutnya  ',
                                style: TextStyle(fontSize: 20),
                                ),
                                new Icon(
                                  Icons.arrow_right,
                                  size: 25.0,
                                )
                            ],
                          ),
                        ),
                      ),
              )

            ],
          ),
      ),
      )

          ),

        );
  }

}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}