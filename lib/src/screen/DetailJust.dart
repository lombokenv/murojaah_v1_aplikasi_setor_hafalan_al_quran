import 'package:flutter/material.dart';

class DetailJus extends StatefulWidget {
   String jus="";
   String image="";

  DetailJus({this.jus,this.image});

  @override
  _DetailJusState createState() => _DetailJusState(jus: jus,image: image);

}

class _DetailJusState extends State<DetailJus> {
  String jus;
  String image;

_DetailJusState({this.jus,this.image});


  @override
  Widget build(BuildContext context) {
        return Scaffold(
          body: new Hero(
          tag: jus,
          child : new NestedScrollView(
              
              headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
                return <Widget>[                 
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: Colors.purple,
                     expandedHeight: 157.0,
                     titleSpacing: 0,
                     flexibleSpace: new FlexibleSpaceBar(
                       centerTitle: true,
                       title: new Wrap(
                          direction: Axis.vertical,
                          children: <Widget>[
                            new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Center(
                                child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text(jus,
                                            style: new TextStyle(
                                           color: Colors.white,
                                            fontSize: 25.0,
                                            fontFamily: "Ahlan",
                                            fontWeight: FontWeight.bold,

                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                ),
                              ]
                            )
                          ]
                       ),
                       background: new Image.asset(
                         image,
                         fit: BoxFit.cover,
                       ),
                     ),
                     automaticallyImplyLeading: true,
                     pinned: true,
                     floating: false,
                     forceElevated: boxIsScrolled,
                  ),
                  SliverPersistentHeader(
                      pinned: true,
                      delegate: _SliverAppBarDelegate(
                        child: PreferredSize(
                        preferredSize: Size.fromHeight(42.0), 
                        child: new Material(
                          elevation: 1.0,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('assets/images/bg2.jpg'),
                                fit: BoxFit.fill
                              )
                            ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Ayat', 
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "Montserrat",color: Colors.white, fontSize: 18.0),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                        ),
                        )
                      )
                      ),
                    ),
                ];
              },
              body: new Container()
              )

          )
        );
  }


}


class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}