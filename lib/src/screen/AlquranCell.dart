import 'package:flutter/material.dart';
import '../model/alquran.dart';

class AlQuranCell extends StatelessWidget {
  const AlQuranCell(this.alquran);
  @required
  final AlQuran alquran;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      color: Colors.purple[300],
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10.0)),
                  child: Hero(
                    tag: "image${alquran.nomor}",
                    child: Image.asset(
                      "assets/images/gridview.jpg",
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                      height: 100,
                    ),
                  ),
                ),
              ),
             
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  "${alquran.nomor}. "+alquran.nama,
                  maxLines: 2,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: Colors.white
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
