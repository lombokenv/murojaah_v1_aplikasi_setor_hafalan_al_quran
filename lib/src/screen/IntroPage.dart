import 'dart:async';
import 'package:flutter/material.dart';
import './home.dart';

class IntroPage extends StatefulWidget{
  @override
  _IntroPageState createState() => _IntroPageState();

}

class _IntroPageState extends State<IntroPage> {
  bool isLoading = true;

  @override
  void initState(){
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
  return new Timer(Duration(seconds: 5), onDoneLoading);
}

onDoneLoading() async {
  Navigator.pop(context,true);// It worked for me instead of above line
  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeState()),);
}

  @override
  Widget build(BuildContext context) {
     return Scaffold(
       body: new Container(
         width: MediaQuery.of(context).size.width,
         height: MediaQuery.of(context).size.height,
         child: 
         Stack(
           children: <Widget>[
             new Container(
               width: MediaQuery.of(context).size.width,
               height: MediaQuery.of(context).size.height,
               child: Image.asset(
                 "assets/images/murojaah.jpg",
                 fit: BoxFit.fill,
               ),
             ),

             new Container(
               width: MediaQuery.of(context).size.width,
               height: MediaQuery.of(context).size.height,
               child: new Column(
                 mainAxisAlignment: MainAxisAlignment.end,
                 children: <Widget>[
                   new Padding(
                     padding: EdgeInsets.only(bottom:0.0),
                     child: new CircularProgressIndicator(
                        backgroundColor: Colors.white,
                     ),
                   ),
                   new Padding(
                     padding: EdgeInsets.only(bottom:50.0, top: 10.0),
                     child: new Text(
                       "Aplikasi sedang di muat \n Mohon menunggu...",
                       style: TextStyle(
                         color: Colors.black,
                         fontSize: 15.0,
                         fontWeight: FontWeight.bold,
                       ),
                       textAlign: TextAlign.center,

                     )
                   )
                
                 ],

               )
             )
           ],
         )
       ),
    
     );
  }
  
}