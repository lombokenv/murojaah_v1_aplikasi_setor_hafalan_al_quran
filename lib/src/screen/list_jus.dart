
import 'package:flutter/material.dart';
import '../model/alquran.dart';
import '../model/services.dart';
import 'dart:async';
import './AlquranCell.dart';
import './GridDetails.dart';

class ListJus extends StatefulWidget {
ListJus() : super();

  @override
  _ListJusState createState() => _ListJusState();

}

class _ListJusState extends State<ListJus> {
  StreamController<int> streamController = new StreamController<int>();

gridview(AsyncSnapshot<List<AlQuran>> snapshot) {
    return Padding(
      padding: EdgeInsets.all(5.0),
      child: GridView.count(
        crossAxisCount: 3,
        childAspectRatio: 1.0,
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        children: snapshot.data.map(
          (alquran) {
            return GestureDetector(
              child: GridTile(
                child: AlQuranCell(alquran),
              ),
              onTap: () {
                goToDetailsPage(context, alquran);
              },
            );
          },
        ).toList(),
      ),
    );
  }

  goToDetailsPage(BuildContext context, AlQuran alquran) {
    Navigator.push(
      context,
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (BuildContext context) => GridDetails(
              curAlquran: alquran,
              url: alquran.audio,
            ),
      ),
    );
  }

  circularProgress() {
    return Center(
      child: const CircularProgressIndicator(),
    );
  }

@override
  Widget build(BuildContext context) {
    return Scaffold(
      body : new NestedScrollView(
              headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
                return <Widget>[                 
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: Colors.purple,
                     expandedHeight: 127.0,
                     titleSpacing: 0,
                     flexibleSpace: new FlexibleSpaceBar(
                       centerTitle: true,
                       title: new Wrap(
                          direction: Axis.vertical,
                          children: <Widget>[
                            new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Center(
                                child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text("Setor Hafalan",
                                            style: new TextStyle(
                                           color: Colors.white,
                                            fontSize: 25.0,
                                            fontFamily: "Ahlan",
                                            fontWeight: FontWeight.bold,

                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                ),
                              
                              ]
                            )
                          ]
                       ),
                       background: new Image.asset(
                         "assets/images/alquran1.jpg",
                         fit: BoxFit.cover,
                       ),
                     ),
                     automaticallyImplyLeading: true,
                     pinned: true,
                     floating: false,
                     forceElevated: boxIsScrolled,
                  ),
                    SliverPersistentHeader(
                      pinned: true,
                      delegate: _SliverAppBarDelegate(
                        child: PreferredSize(
                        preferredSize: Size.fromHeight(42.0), 
                        child: new Material(
                          elevation: 1.0,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage('assets/images/bg2.jpg'),
                                fit: BoxFit.fill
                              )
                            ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'PILIHAN SURAH', 
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "Montserrat",color: Colors.white, fontSize: 18.0),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                        ),
                        )
                      )
                      ),
                    ),
                ];
              },        
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                    child: FutureBuilder<List<AlQuran>>(
                      future: Services.getArti(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return Text('Error ${snapshot.error}');
                        }
                        //
                        if (snapshot.hasData) {
                          streamController.sink.add(snapshot.data.length);
                          // gridview
                          return gridview(snapshot);
                        }

                        return circularProgress();
                      },
                    ),
                  ),
                ],
              ),
      )
    );
  }

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

}


class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({ this.child });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => child.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }

}